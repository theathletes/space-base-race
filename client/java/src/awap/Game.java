package awap;

import java.util.*;

import com.google.common.base.Optional;

public class Game {
	private State state;
	private Integer number;

	public Optional<Move> updateState(State newState) {
		if (newState.getError().isPresent()) {
			Logger.log(newState.getError().get());
			return Optional.absent();
		}

		if (newState.getMove() != -1) {
			return Optional.fromNullable(findMove());
		}

		state = newState;
		if (newState.getNumber().isPresent()) {
            Logger.log("Number Set");
			number = newState.getNumber().get();
        } else {
            state.setNumber(number);
        }

		return Optional.absent();
	}

	private Move findMove() {
        
        /*Move openingMove = Opening.returnMove(state);
        if(openingMove != null) {
            return openingMove;
                    }
         */
    int N = state.getDimension();
    List<Block> blocks = state.getBlocks().get(number);
    List<Point> playablePoints = state.getPlayableLocations();
        List<Move> possibleMoves = new ArrayList<Move>();
        List<Integer> values = new ArrayList<Integer>();
    int valueCounter = 0;

    for(int p = 0; p < playablePoints.size(); p++) {
      int x = playablePoints.get(p).getX();
      int y = playablePoints.get(p).getY();
      for (int rot = 0; rot < 4; rot++) {
        for (int i = 0; i < blocks.size(); i++) {
          if (canPlace(blocks.get(i).rotate(rot), new Point(x, y))) {
            Move candidate = new Move(i, rot, x, y);
            possibleMoves.add(candidate);
              values.add(getMoveValue(candidate));
            
          }
        }
      }
    }
        int max = 0;
        for(int i = 0; i < values.size(); i++) {
            if(values.get(i) > values.get(max)) {
                max = i;
            }
        }
		return possibleMoves.get(max);
  

		// int N = state.getDimension();
		// List<Block> blocks = state.getBlocks().get(number);
  //       state.logBoard();
		// for (int x = 0; x < N; x++) {
		// 	for (int y = 0; y < N; y++) {
		// 		for (int rot = 0; rot < 4; rot++) {
		// 			for (int i = 0; i < blocks.size(); i++) {
		// 				if (canPlace(blocks.get(i).rotate(rot), new Point(x, y))) {
  //                           Logger.log(String.format("%d,%d,%d,%d", x, y, rot, i));
		// 					return new Move(i, rot, x, y);
		// 				}
		// 			}
		// 		}
		// 	}
		// }
	// 	return new Move(0, 0, 0, 0);
	// }
  }

  private int getMoveValue(Move nextMove) {
    int blockNum = nextMove.getIndex();
    Block block = state.getBlocks().get(number).get(blockNum);
    List<Point> offsets = block.getOffsets();
    int blockWeight = offsets.size();
    int[] edgeCorner = getEdgeCorner(nextMove);
    int edges = edgeCorner[0];
    int corners = edgeCorner[1];

    return blockWeight + edges + corners;
  }

  private int[] getEdgeCorner(Move nextMove) {
    int N = state.getDimension();
    int blockNum = nextMove.getIndex();
    int rotation = nextMove.getRotations();
    List<List<Integer>> board = state.getBoard();
    Block block = state.getBlocks().get(number).get(blockNum);
    List<Point> offsets = block.getOffsets();
    int absX = nextMove.getX();
    int absY = nextMove.getY();
    int edges = 0;
    int corners = 0;

    for(int i = 0; i < offsets.size(); i++) {
      Point cand = offsets.get(i).rotate(rotation);
      int offsetX = cand.getX();
      int offsetY = cand.getY();

      // -1,-1
      int y = absY+offsetY-1;
      int x = absX+offsetX-1;
      if((y == -1 && x == -1) || (y >= 0 && x >= 0)) {
        if(!notPossible(x,y) && realCorner(offsets, rotation, offsetX-1, offsetY-1)) corners++;
      }

      // -1,0
      y = absY+offsetY-1;
      x = absX+offsetX;
      if(y >= -1) {
        if(notPossible(x,y)) edges++;
      }

      // -1,1
      y = absY+offsetY-1;
      x = absX+offsetX+1;
      if((y == -1 && x == N+1) || (y >= 0 && x <= N)) {
        if(!notPossible(x,y) && realCorner(offsets, rotation, offsetX-1, offsetY+1)) corners++;
      }

      // 0,-1
      y = absY+offsetY;
      x = absX+offsetX-1;
      if(x >= -1) {
        if(notPossible(x,y)) edges++;
      }

      // 0,1
      y = absY+offsetY;
      x = absX+offsetX+1;
      if(x <= N+1) {
        if(notPossible(x,y)) edges++;
      }

      // 1,-1
      y = absY+offsetY+1;
      x = absX+offsetX-1;
      if((y == N+1 && x == -1) || (y <= N && x >= 0)) {
        if(!notPossible(x,y) && realCorner(offsets, rotation, offsetX+1, offsetY-1)) corners++;
      }

      // 1,0
      y = absY+offsetY+1;
      x = absX+offsetX;
      if(y <= N+1) {
        if(notPossible(x,y)) edges++;
      }

      // 1,1
      y = absY+offsetY+1;
      x = absX+offsetX+1;
      if((x == N+1 && y == N+1) || (y <= N && x <= N)) {
        if(!notPossible(x,y) && realCorner(offsets, rotation, offsetX+1, offsetY+1)) corners++;
      }
    }

    int[] result = {edges, corners};
    return result;
  }

  private boolean notPossible(int x,int y) {
    int N = state.getDimension();
    List<List<Integer>> board = state.getBoard();
    return (x < 0) || (y < 0) || (x > N) || (y > N) || (board.get(y).get(x) != -1);
  }

  private boolean realCorner(List<Point> offsets, int rotation, int newOSX, int newOSY) {
    for(int i = 0; i < offsets.size(); i++) {
      Point cand = offsets.get(i).rotate(rotation);
      int offsetX = cand.getX();
      int offsetY = cand.getY();
      if(newOSX == offsetX && newOSY == offsetY) return false;
    }
    return true;
  }

	private int getPos(int x, int y) {
		return state.getBoard().get(x).get(y);
	}

	private boolean canPlace(Block block, Point p) {
		boolean onAbsCorner = false, onRelCorner = false;
		int N = state.getDimension() - 1;

		Point[] corners = { new Point(0, 0), new Point(N, 0), new Point(N, N),
				new Point(0, N) };
		;
		Point corner = corners[number];

		for (Point offset : block.getOffsets()) {
			Point q = offset.add(p);
			int x = q.getX(), y = q.getY();

			if (x > N || x < 0 || y < 0 || y > N
          || getPos(x, y) >= 0
          || getPos(x, y) == -2
					|| (x > 0 && getPos(x - 1, y) == number)
					|| (y > 0 && getPos(x, y - 1) == number)
					|| (x < N && getPos(x + 1, y) == number)
					|| (y < N && getPos(x, y + 1) == number)) {
				return false;
			}

			onAbsCorner = onAbsCorner || q.equals(corner);
			onRelCorner = onRelCorner
					|| (x > 0 && y > 0 && getPos(x - 1, y - 1) == number)
					|| (x < N && y > 0 && getPos(x + 1, y - 1) == number)
					|| (x > 0 && y < N && getPos(x - 1, y + 1) == number)
					|| (x < N && y < N && getPos(x + 1, y + 1) == number);
		}

		return !((getPos(corner.getX(), corner.getY()) < 0 && !onAbsCorner) || (!onAbsCorner && !onRelCorner));
	}
}
