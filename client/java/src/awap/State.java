package awap;

import java.util.List;
import java.util.Map;
import static java.lang.System.*;
import java.util.ArrayList;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;

public class State {
	private Optional<String> error = Optional.absent();
	private Optional<Integer> number = Optional.absent();
	private List<List<Integer>> board;
	private List<List<Block>> blocks;
	private int dimension;
	private int turn;
	private int move = -1;
	private String url;
	private List<List<Integer>> bonusSquares;
    private List<Point> playableLocations = null;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Optional<String> getError() {
		return error;
	}

	public void setError(String error) {
		this.error = Optional.fromNullable(error);
	}

	public Optional<Integer> getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = Optional.fromNullable(number);
	}

	public List<List<Integer>> getBoard() {
		return board;
	}

	@SuppressWarnings("unchecked")
	public void setBoard(Map<String, Object> board) {
		this.board = (List<List<Integer>>) board.get("grid");
    this.bonusSquares = (List<List<Integer>>) board.get("bonus_squares");
		this.setDimension((int) board.get("dimension"));
        playableLocations = null;
        //logBoard();
	}
    
    public boolean[][] bonusGrid() {
        boolean[][] bonusGrid = new boolean[board.size()][board.get(0).size()];
        for(List<Integer> bonusLocation: this.bonusSquares) {
            bonusGrid[bonusLocation.get(0)][bonusLocation.get(1)] = true;
        }
        return bonusGrid;
    }
    
    public void logBoard() {
        String output = "   ";
        for(int c = 0; c < this.board.get(0).size(); c++) {
            output+= String.format(" %4d ", c);
        }
        Logger.log(output);
        output = "";
        
        boolean[][] bonusGrid = bonusGrid();
        
        for(int r = 0; r < this.board.size(); r++) {
            output += String.format("%3d: ", r);
            for(int c = 0; c < this.board.get(r).size(); c++) {
                String bonus = bonusGrid[r][c] ? "*" : " ";
                String playable = this.getPlayableLocations().contains(new Point(r, c)) ? "-" : " ";
                
                output += String.format("%s%s%2d%s%s", playable, bonus, board.get(r).get(c), bonus, playable);
            }
            Logger.log(output);
            output = "";
        }
    }

	public int getMove() {
		return move;
	}

	public void setMove(int move) {
		this.move = move;
	}

	public int getDimension() {
		return dimension;
	}

	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}

	public List<List<Block>> getBlocks() {
		return blocks;
	}

	public void setBlocks(List<List<List<Map<String, Integer>>>> blocks) {
		List<List<Block>> blockList = Lists.newArrayList();
		for (List<List<Map<String, Integer>>> player : blocks) {
			List<Block> playerList = Lists.newArrayList();
			for (List<Map<String, Integer>> block : player) {
				playerList.add(new Block(block));
			}
			blockList.add(playerList);
		}

		this.blocks = blockList;
	}

	public List<List<Integer>> getBonusSquares() {
		return bonusSquares;
	}
    
    public int getPos(int x, int y) {
        return this.getBoard().get(x).get(y);
    }
    
    public List<Point> getPlayableLocations() {
        if(playableLocations == null) calculatePlayableLocations();
        return playableLocations;
    }
    
    private void calculatePlayableLocations() {
        if(!this.getNumber().isPresent()) {
            Logger.log("Missing Number");
        }
        this.playableLocations = new ArrayList<Point>();
        for(int r = 0; r < this.board.size(); r++) {
            for(int c = 0; c < this.board.get(r).size(); c++) {
                Point p = new Point(r, c);
                if(Block.canPlace(Block.SINGLE, p, this)) {
                    playableLocations.add(p);
                }
            }
        }
        Logger.log(playableLocations.toString());
    }
    

  public void setPlayers(List<String> players) {}
}
