/*package awap;

import java.util.*;

public class Opening {
    //elongated s
    public static Block setup1(){
        List<Point> points=new ArrayList<Point>();
        points.add(new Point(0,0));
        points.add(new Point(1,0));
        points.add(new Point(1,1));
        points.add(new Point(2,1));
        points.add(new Point(3,1));
        Block piece=new Block(points, false);
        return piece;
    }
    
    //Z shape
    public static Block setup2(){
        List<Point> points=new ArrayList<Point>();
        points.add(new Point(0,0));
        points.add(new Point(1,0));
        points.add(new Point(1,1));
        points.add(new Point(2,1));
        points.add(new Point(3,1));
        Block piece=new Block(points, false);
        return piece;
    }
    //corner capper
    public static Block setup3(){
        List<Point> points=new ArrayList<Point>();
        points.add(new Point(0,0));
        points.add(new Point(1,0));
        points.add(new Point(0,1));
        points.add(new Point(0,2));
        points.add(new Point(1,2));
        Block piece=new Block(points, false);
        return piece;
    }
    //big L
    public static Block setup4(){
        List<Point> points=new ArrayList<Point>();
        points.add(new Point(0,0));
        points.add(new Point(1,0));
        points.add(new Point(2,0));
        points.add(new Point(3,0));
        points.add(new Point(0,1));
        Block piece=new Block(points, false);
        return piece;
    }
    //little line
    public static Block setup5(){
        List<Point> points=new ArrayList<Point>();
        points.add(new Point(0,0));
        points.add(new Point(1,0));
        points.add(new Point(2,0));
        points.add(new Point(4,0));
        Block piece=new Block(points, false);
        return piece;
    }
    
    public static Move returnMove(State s){
        final Block piece1=setup1();
        final Block piece2=setup2();
        final Block piece3=setup3();
        final Block piece4=setup4();
        final Block piece5=setup5();
        int index;
        if (s.getTurn()==0) {
            List<Block> myBlocks=s.getBlocks().get(s.getNumber().get());
            for(Block b: myBlocks) {
                for (Point a : b.getOffsets()) {
                    //somebody will need to write this in block class
                    //I won't do iteration through points here...
                    if (a.equals(piece1)) {
                        index=b.getOffsets().indexOf(piece1);
                    }
                }
            }
            return new Move(index,1,0,0);
        }
        if (s.getTurn()==1) {
            List<Block> myBlocks=s.getBlocks().get(s.getNumber().get());
            for(Block b: myBlocks) {
                for (Point a : b.getOffsets()) {
                    //somebody will need to write this in block class
                    //I won't do iteration through points here...
                    if (a.equals(piece2)) {
                        index=a.indexOf(piece2);
                    }
                }
            }

            return new Move(index,1,4,2);
        }
        
        if (s.getTurn()==2){
            List<Block> myBlocks=s.getBlocks().get(s.getNumber().get());
            for(Block b: myBlocks) {
                for (Point a : b.getOffsets()) {
                    //somebody will need to write this in block class
                    //I won't do iteration through points here...
                    if (a.equals(piece3)) {
                        index=a.indexOf(piece3);
                    }
                }
            }
            return new Move(index,1,7,5);
        }
        
        if (s.getTurn()==3){
            List<Block> myBlocks=s.getBlocks().get(s.getNumber().get());
            for(Block b: myBlocks) {
                for (Point a : b.getOffsets()) {
                    //somebody will need to write this in block class
                    //I won't do iteration through points here...
                    if (a.equals(piece4)) {
                        index=a.indexOf(piece4);
                    }
                }
            }

            Point p1 =new Point(10,2);
            Point p2 = new Point(9,8);
            if(!canPlace(s.getBlocks().get(s.getNumber().get()).get(index),p1)
               && !canPlace(s.getBlocks().get(s.getNumber().get()).get(index),p2)){
                return null;
                //call territory;
                //return and break out
            }
            else if (game.getBoard()[10][2]==-1){
                return new Move(index,3,7,2);
            }
            else{
                return new Move(index,3,9,8);
            }
        }
        if (s.getTurn()==4){
            List<Block> myBlocks=s.getBlocks().get(s.getNumber().get());
            for(Block b: myBlocks) {
                for (Point a : b.getOffsets()) {
                    //somebody will need to write this in block class
                    //I won't do iteration through points here...
                    if (a.equals(piece5)) {
                        index=a.indexOf(piece5);
                    }
                }
            }

            Point p1=new Point(6,8);
            if(!canPlace(s.getBlocks().get(s.getNumber().get()).get(index), p1)){
                return null;
                //call territory;
            }
            else{
                return new Move(index,0,6,8);
            }
        }
        return null;
    }
}
*/