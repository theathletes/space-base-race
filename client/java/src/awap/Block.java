package awap;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

public class Block {
	private List<Point> offsets;
    
    static final Block SINGLE;
    static {
        SINGLE = new Block();
        List<Point> shape = new ArrayList<Point>();
        shape.add(new Point(0, 0));
        SINGLE.setOffsets(shape);
    }

	public Block() {
	}

	public Block(List<Map<String, Integer>> offsets) {
		this.offsets = Lists.transform(offsets,
				new Function<Map<String, Integer>, Point>() {
					public Point apply(Map<String, Integer> map) {
						return new Point(map);
					}
				});
	}
    public Block(List<Point> offsets, boolean badData) {
        this.offsets = offsets;
    }

	public List<Point> getOffsets() {
		return offsets;
	}

	public void setOffsets(List<Point> offsets) {
		this.offsets = offsets;
	}

	public Block rotate(final int rotations) {
		if (rotations == 0) {
			return this;
		}

		Block block = new Block();
		block.setOffsets(Lists.transform(offsets, new Function<Point, Point>() {
			public Point apply(Point p) {
				return p.rotate(rotations);
			}
		}));

		return block;
	}
    
    
    public static boolean canPlace(Block block, Point p, State state) {
        boolean onAbsCorner = false, onRelCorner = false;
        int N = state.getDimension() - 1;
        
        Point[] corners = { new Point(0, 0), new Point(N, 0), new Point(N, N),
            new Point(0, N) };
        ;
        
        if(!state.getNumber().isPresent()) {
            //Logger.log("Number Missing");
            return false;
        }
        
        int number = state.getNumber().get();
    
        Point corner = corners[number];
        
        
        for (Point offset : block.getOffsets()) {
            Point q = offset.add(p);
            int x = q.getX(), y = q.getY();
            
            if (x > N || x < 0 || y < 0 || y > N
                || state.getPos(x, y) >= 0
                || state.getPos(x, y) == -2
                || (x > 0 && state.getPos(x - 1, y) == number)
                || (y > 0 && state.getPos(x, y - 1) == number)
                || (x < N && state.getPos(x + 1, y) == number)
                || (y < N && state.getPos(x, y + 1) == number)) {
                return false;
            }
            
            onAbsCorner = onAbsCorner || q.equals(corner);
            onRelCorner = onRelCorner
            || (x > 0 && y > 0 && state.getPos(x - 1, y - 1) == number)
            || (x < N && y > 0 && state.getPos(x + 1, y - 1) == number)
            || (x > 0 && y < N && state.getPos(x - 1, y + 1) == number)
            || (x < N && y < N && state.getPos(x + 1, y + 1) == number);
        }
        
        return !((state.getPos(corner.getX(), corner.getY()) < 0 && !onAbsCorner) || (!onAbsCorner && !onRelCorner));
    }
}
